// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![no_std]

extern crate alloc;
extern crate core;
extern crate lens_flows;
extern crate lens_system;
extern crate postcard;
extern crate serde;

use alloc::boxed::Box;
use alloc::fmt::Display;
use alloc::vec::Vec;
use lens_flows::{object, packet};
use lens_system::{Error, Result};

#[derive(Debug)]
struct PostcardError(postcard::Error);

impl Error for PostcardError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

impl Display for PostcardError {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        self.0.fmt(f)
    }
}

static SERIALIZE_MAX: usize = 65536;

/// An implementation of object::Send that uses the Postcard crate.
pub struct PostcardSendFilter<'a> {
    flow: &'a dyn packet::Send,
}

impl<'a> PostcardSendFilter<'a> {
    /// Create a `PostcardSendFilter` that wraps the given `flow`.
    pub fn new(flow: &'a dyn packet::Send) -> PostcardSendFilter {
        PostcardSendFilter { flow }
    }
}

impl<'a, T: serde::Serialize> object::Send<T> for PostcardSendFilter<'a> {
    fn send(&self, object: T) -> Result<()> {
        let mut buf: Vec<u8> = Vec::with_capacity(SERIALIZE_MAX);
        buf.resize(SERIALIZE_MAX, 0);
        match self
            .flow
            .send(match postcard::to_slice(&object, &mut buf[..]) {
                Ok(used) => &mut used[..],
                Err(err) => {
                    return Err(Box::new(PostcardError(err)));
                }
            }) {
            Ok(()) => Ok(()),
            Err(err_box) => Err(err_box),
        }
    }
}

/// An implementation of Receive that uses the Postcard crate.
pub struct PostcardReceiveFilter<'a> {
    flow: &'a dyn packet::Receive,
}

impl<'a> PostcardReceiveFilter<'a> {
    /// Create a `PostcardReceiveFilter` that wraps the given `flow`.
    pub fn new(flow: &'a dyn packet::Receive) -> PostcardReceiveFilter {
        PostcardReceiveFilter { flow }
    }
}

impl<'a, T: serde::de::DeserializeOwned> object::Receive<T> for PostcardReceiveFilter<'a> {
    fn receive(&self) -> Result<T> {
        match self.flow.receive(SERIALIZE_MAX) {
            // Received buffer gets thrown away...
            Ok(bytes) => match postcard::from_bytes::<T>(&bytes[..]) {
                // which angers rustc here.
                Ok(obj) => Ok(obj),
                Err(err) => Err(Box::new(PostcardError(err))),
            },
            Err(err_box) => Err(err_box),
        }
    }
}
